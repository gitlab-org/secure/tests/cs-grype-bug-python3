FROM ubuntu:18.04

RUN apt-get update && \
    apt-get upgrade -y && \
    apt install -y \
        python3 \
        python3-pip \
        python3-dev \
        ca-certificates \
        git build-essential cmake
